<?php

/**
 * @param $days
 * @param $day
 * @return string
 */
function checkDay($days, $day)
{
    foreach ($days as $dayObj)
    {
        if ($dayObj->day == $day)
        {
            return 'checked="checked"';
        }
    }

    return '';
}

/**
 * @param $errors
 * @param $name
 * @return string
 */
function getErrorClass($errors, $name)
{
    if ($errors->first($name))
    {
        return 'error-input';
    }

    return '';
}

/**
 * @param $type
 * @param $text
 * @return string
 */
function showMessage($type, $text)
{
    switch ($type)
    {
        case 'info':
            return '<div class="cws_msg_box info-box closable clearfix" style="background-color:#3eb2cf">
                <div class="icon_section"><i class="fa fa-info" style="color:#3eb2cf;"></i></div>
                <div class="content_section">
                    <div class="msg_box_title">Info</div>
                    <div class="msg_box_text">' . $text . '</div>
                </div>
                <div class="cls_btn"></div>
            </div>';
            break;

        case 'success':
            return '<div class="cws_msg_box success-box closable clearfix">
                <div class="icon_section"><i class="fa fa-thumbs-up"></i></div>
                <div class="content_section">
                    <div class="msg_box_title">Success</div>
                    <div class="msg_box_text">' . $text . '</div>
                </div>
                <div class="cls_btn"></div>
            </div>';
            break;

        case 'warning':
            return '<div class="cws_msg_box warning-box closable clearfix">
                <div class="icon_section"><i class="fa fa-bolt"></i></div>
                <div class="content_section">
                    <div class="msg_box_title">Warning</div>
                    <div class="msg_box_text">' . $text . '</div>
                </div>
                <div class="cls_btn"></div>
            </div>';
            break;

        case 'error':
            return '<div class="cws_msg_box error-box closable clearfix">
                <div class="icon_section"><i class="fa fa-exclamation"></i></div>
                <div class="content_section">
                    <div class="msg_box_title">Error</div>
                    <div class="msg_box_text">' . $text . '</div>
                </div>
                <div class="cls_btn"></div>
            </div>';
            break;
    }
}