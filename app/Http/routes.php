<?php

Route::get('/', function()
{
    return ( ! Auth::check()) ? Redirect::to('dashboard') : Redirect::to('auth/login');
});

/**
 * Auth
 */
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => 'auth'], function() {

    /**
     * Admin routes
     */
    Route::group(['middleware' => ['auth', 'admin']], function()
    {
        /**
         * Dashboard
         */
        Route::get('dashboard', 'DashboardController@index');

        /**
         * Families
         */
        Route::get('families', 'FamiliesController@index');
        Route::post('families/store', 'FamiliesController@store');
        Route::get('families/search/{q}', 'FamiliesController@search');
        Route::get('family/{familyId}', 'FamiliesController@manage')
            ->where('familyId', '\d+');
        Route::get('family/{familyId}/delete', 'FamiliesController@delete')
            ->where('familyId', '\d+');
        Route::post('families/find-by-last-name-ajax', 'FamiliesController@findByLastNameAjax');

        /**
         * Parents
         */
        Route::post('parents/store', 'ParentsController@store');
        Route::get('parent/{parentId}', 'ParentsController@edit')
            ->where('parentId', '\d+');
        Route::post('parent/{parentId}/update', 'ParentsController@update')
            ->where('parentId', '\d+');
        Route::get('parent/{parentId}/delete', 'ParentsController@delete')
            ->where('parentId', '\d+');

        /**
         * Children
         */
        Route::post('children/store', 'ChildrenController@store');
        Route::get('child/{childId}', 'ChildrenController@edit')
            ->where('childId', '\d+');
        Route::post('child/{childId}/update', 'ChildrenController@update')
            ->where('childId', '\d+');
        Route::get('child/{childId}/delete', 'ChildrenController@delete')
            ->where('childId', '\d+');

        /**
         * Groups
         */
        Route::get('groups', 'GroupsController@index');
        Route::post('groups/store', 'GroupsController@store');

        /**
         * Payments
         */
        Route::get('payments', 'PaymentsController@index');
        Route::post('payments/store', 'PaymentsController@store');
        Route::get('payment/{paymentId}/delete', 'PaymentsController@delete')
            ->where('paymentId', '\d+');
    });

    Route::post('children/find-by-name-ajax', 'ChildrenController@findByNameAjax');

    /**
     * Attendance
     */
    Route::get('attendance', 'AttendanceController@calendar');
    Route::get('attendance/get-scheduled-ajax', 'AttendanceController@getScheduledAjax');
    Route::get('attendance/day/{day}', 'AttendanceController@day')
        ->where('day', '\d{4}-\d{2}-\d{2}');
    Route::post('attendance/update-ajax', 'AttendanceController@updateAjax');
    Route::post('attendance/add-child-ajax', 'AttendanceController@addChildAjax');
});

Route::get('t', function ()
{
    return View::make('blank');
});
