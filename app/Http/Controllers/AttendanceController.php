<?php namespace App\Http\Controllers;

use Childcare\Attendance\Attendance;
use Childcare\Groups\Group;
use Input;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Response;
use Session;

/**
 * Class AttendanceController
 * @package App\Http\Controllers
 */
class AttendanceController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function calendar()
    {
        $calendar = Calendar::setOptions([
            'eventSources' =>  [
                '/attendance/get-scheduled-ajax',
            ]
        ]);

        return view('admin.attendance.calendar', compact('calendar'));
    }

    /**
     * @return mixed
     */
    public function getScheduledAjax()
    {
        $input = Input::only('start', 'end');

        $scheduled = (new Attendance())->scheduledAttendance($input['start'], $input['end']);

        return Response::json($scheduled);
    }

    /**
     * @param $day
     * @return \Illuminate\View\View
     */
    public function day($day)
    {
        list($groupIds, $children) = Attendance::day($day);

        $groups = Group::getById($groupIds);

        $groupsList = [0 => 'Select Group'];

        foreach ($groups as $group)
        {
            $groupsList[$group->name] = $group->name;
        }

        $allGroups = (new Group())->getForSelect();

        $teachers[1] = 'Susan';

        return view('admin.attendance.day',
            compact('day', 'children', 'groups', 'teachers', 'groupsList', 'allGroups'));
    }

    /**
     * Update attendance
     */
    public function updateAjax()
    {
        $input = Input::except('_token');

        Attendance::updateForDay($input);
    }

    /**
     * @return bool
     */
    public function addChildAjax()
    {
        $input = Input::only('date', 'childId');

        $attendance = Attendance::addChildToDate($input['date'], $input['childId']);

        if ($attendance->exists)
        {
            Session::flash('added-child-id', $attendance->child_id);
        }

        return Response::json([]);
    }
}