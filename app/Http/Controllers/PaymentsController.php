<?php namespace App\Http\Controllers;

use App;
use App\Childcare\Payments\Payment;
use App\Http\Requests;
use Input;
use Redirect;
use Session;

/**
 * Class PaymentsController
 * @package App\Http\Controllers
 */
class PaymentsController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $payments = (new Payment())->getAll();

        return view('admin.payments.index', compact('payments'));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = Input::only('family_id', 'type', 'amount', 'notes');

        $payment = Payment::create($input);

        if ($payment->getErrors())
        {
            Session::flash('error-form', 'create-payment-form');

            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to create the payment, please check your input and try again'
            ]);

            return Redirect::back()->withInput()->withErrors($payment->getErrors());
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => 'Payment created successfully'
        ]);

        return Redirect::back();
    }

    /**
     * @param $paymentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($paymentId)
    {
        $family = Payment::find($paymentId);

        if ( ! $family) App::abort(404);

        Payment::whereid($paymentId)->delete();

        Session::flash('message', [
            'type' => 'success',
            'text' => 'Payment successfully deleted'
        ]);

        return Redirect::back();
    }

}
