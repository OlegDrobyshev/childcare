<?php namespace App\Http\Controllers;

use App;
use Carbon\Carbon;
use Childcare\Children\Child;
use Childcare\Groups\Group;
use Input;
use Redirect;
use Response;
use Session;

/**
 * Class ChildrenController
 * @package App\Http\Controllers
 */
class ChildrenController extends Controller
{
    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = Input::only(
            'family_id', 'group_id', 'name', 'gender', 'dob', 'photoFile', 'attendance',
            'full_day', 'allergies', 'notes', 'weekly_rate', 'discount'
        );

        if ($input['dob'])
        {
            $input['dob'] = Carbon::createFromTimestamp(strtotime($input['dob']))
                ->format('Y-m-d H:i:s');
        }

        if ( ! $input['group_id'])
        {
            unset($input['group_id']);
        }

        $child = (new Child())->createChild($input);

        if ($child->getErrors())
        {
            Session::flash('error-form', 'add-child-form');

            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to create child, please check your input and try again'
            ]);

            return Redirect::back()->withInput()->withErrors($child->getErrors());
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => $child->full_name . ' successfully added '
                . '<a href="/child/' . $child->id . '" class="notification-link"> <i class="fa fa-pencil"></i> Edit Child</a>'
        ]);

        return Redirect::back();
    }

    /**
     * @param $childId
     * @return \Illuminate\View\View
     */
    public function edit($childId)
    {
        $child = Child::find($childId);

        if ( ! $child) App::abort(404);

        $groups = (new Group)->getForSelect();

        return view('admin.children.edit', compact('child', 'groups'));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $input = Input::only(
            'family_id', 'child_id', 'group_id', 'name', 'gender', 'dob', 'attendance',
            'photoFile', 'full_day', 'allergies', 'notes', 'weekly_rate', 'discount'
        );

        if ($input['dob'])
        {
            $input['dob'] = Carbon::createFromTimestamp(strtotime($input['dob']))
                ->format('Y-m-d H:i:s');
        }

        if ( ! $input['group_id'])
        {
            unset($input['group_id']);
        }

        $child = (new Child())->updateChild($input);

        if ($child->getErrors())
        {
            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to update the child, please check your input and try again'
            ]);

            return Redirect::back()->withInput()->withErrors($child->getErrors());
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => $child->full_name . ' successfully updated. '
                . '<a href="/family/' . $child->family_id . '" class="notification-link"><i class="fa fa-chevron-left"></i> BACK</a>'
        ]);

        return Redirect::back();
    }

    /**
     * @param $childId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($childId)
    {
        $child = Child::find($childId);

        if ( ! $child) App::abort(404);

        Child::find($childId)->deletePhoto();
        Child::find($childId)->delete();

        Session::flash('message', [
            'type' => 'success',
            'text' => $child->full_name . ' successfully deleted'
        ]);

        return Redirect::back();
    }

    /**
     * @return mixed
     */
    public function findByNameAjax()
    {
        $input = Input::only('term');

        $data = (new Child())->searchByName($input['term']);

        return Response::json($data);
    }
}