<?php namespace App\Http\Controllers;

use Childcare\Families\Family;
use Childcare\Children\Child;
use App\Childcare\Payments\Payment;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function index()
	{
        $totalFamilies = Family::count();

        $totalChildren = Child::count();

        $totalBilled = Payment::totalBilled();

		return view('admin.dashboard', compact(
            'totalFamilies', 'totalChildren', 'totalBilled'
        ));
	}

}
