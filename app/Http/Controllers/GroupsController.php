<?php namespace App\Http\Controllers;

use Childcare\Groups\Group;
use Input;
use Redirect;
use Session;

/**
 * Class GroupsController
 * @package App\Http\Controllers
 */
class GroupsController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $groups = Group::all();

        return view('admin.groups.index', compact('groups'));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = Input::only('name');

        $group = Group::create([
            'name' => $input['name']
        ]);

        if ($group->getErrors())
        {
            Session::flash('error-form', 'create-group-form');

            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to create the group, please check your input and try again'
            ]);

            return Redirect::back()->withInput();
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => ucfirst($group->name) . ' group created successfully'
        ]);

        return Redirect::back();
    }

}