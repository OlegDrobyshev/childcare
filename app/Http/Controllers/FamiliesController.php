<?php namespace App\Http\Controllers;

use App;
use Childcare\Families\Family;
use Childcare\Groups\Group;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Input;
use Response;

/**
 * Class FamiliesController
 * @package App\Http\Controllers
 */
class FamiliesController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function index()
	{
        $model = new Family();

        $families = $model->getAll();

		return view('admin.families.index', compact('families'));
	}

    /**
     * @return mixed
     */
    public function store()
    {
        $input = Input::only('last_name');

        $family = Family::create([
            'last_name' => $input['last_name']
        ]);

        if ($family->getErrors())
        {
            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to create the family, please check your input and try again'
            ]);

            return Redirect::back()->withInput();
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => ucfirst($family->last_name) . ' family created successfully, now you can '
                . '<a href="/family/' . $family->id . '" class="notification-link"><i class="fa fa-users"></i> Add Members</a>'
        ]);

        return Redirect::back();
    }

    /**
     * @param $familyId
     * @return mixed
     */
    public function delete($familyId)
    {
        $family = Family::find($familyId);

        if ( ! $family) App::abort(404);

        Family::whereid($familyId)->delete();

        Session::flash('message', [
            'type' => 'success',
            'text' => 'The ' . $family->last_name . 's successfully deleted'
        ]);

        return Redirect::back();
    }

    /**
     * @param $searchString
     * @return \Illuminate\View\View
     */
    public function search($searchString)
    {
        $model = new Family();

        $families = $model->search($searchString);

        return view('admin.families.index',
            compact('families', 'searchString'));
    }

    /**
     * @param $familyId
     * @return \Illuminate\View\View|void
     */
    public function manage($familyId)
    {
        $family = Family::find($familyId);

        if ( ! $family) App::abort('404');

        $parents = $family->parents()->orderBy('name')->get();

        $children = $family->children()->orderBy('name')->get();

        $groups = (new Group())->getForSelect();

        return view('admin.families.manage',
            compact('family', 'parents', 'children', 'groups'));
    }

    /**
     * @return mixed
     */
    public function findByLastNameAjax()
    {
        $input = Input::only('term');

        $data = (new Family())->searchByLastName($input['term']);

        return Response::json($data);
    }

}
