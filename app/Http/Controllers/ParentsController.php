<?php namespace App\Http\Controllers;

use App;
use Childcare\Parents\Parents;
use Input;
use Redirect;
use Session;

/**
 * Class ParentsController
 * @package App\Http\Controllers
 */
class ParentsController extends Controller
{

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = Input::only('name', 'email', 'phone', 'address', 'family_id');

        $parent = Parents::create($input);

        if ($parent->getErrors())
        {
            Session::flash('error-form', 'add-parent-form');

            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to add parent, please check your input and try again'
            ]);

            return Redirect::back()->withInput()->withErrors($parent->getErrors());
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => $parent->full_name . ' successfully added '
                . '<a href="/parent/' . $parent->id . '" class="notification-link"> <i class="fa fa-pencil"></i> Edit Parent</a>'
        ]);

        return Redirect::back();
    }

    /**
     * @param $parentId
     * @return \Illuminate\View\View
     */
    public function edit($parentId)
    {
        $parent = Parents::find($parentId);

        if ( ! $parent) App::abort(404);

        return view('admin.parents.edit', compact('parent'));
    }

    /**
     * @param $parentId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($parentId)
    {
        $parent = Parents::find($parentId);

        if ( ! $parent) App::abort(404);

        $input = Input::only('name', 'email', 'phone', 'address');

        foreach ($input as $column => $value)
        {
            $parent->{$column} = $value;
        }

        $parent->save();

        if ($parent->getErrors())
        {
            Session::flash('message', [
                'type' => 'error',
                'text' => 'Wasn\'t able to update parent, please check your input and try again'
            ]);

            return Redirect::back()->withInput()->withErrors($parent->getErrors());
        }

        Session::flash('message', [
            'type' => 'success',
            'text' => $parent->full_name . ' successfully updated'
        ]);

        return Redirect::back();
    }

    /**
     * @param $parentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($parentId)
    {
        $parent = Parents::find($parentId);

        if ( ! $parent) App::abort(404);

        Parents::whereId($parentId)->delete();

        Session::flash('message', [
            'type' => 'success',
            'text' => $parent->full_name . ' successfully deleted '
        ]);

        return Redirect::back();
    }
}