<?php namespace Childcare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * Class BaseModel
 * @package Childcare
 */
class BaseModel extends Model
{
    /**
     * Stores errors
     *
     * @var
     */
    protected $errors;

    /**
     * @var array
     */
    public static $rules = [];

    /**
     * @var array
     */
    public static $errorMessages = [];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            return $model->validate();
        });
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = Validator::make($this->getAttributes(), static::$rules, static::$errorMessages);

        if ($validation->fails())
        {
            $this->errors = $validation->messages();

            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }
}