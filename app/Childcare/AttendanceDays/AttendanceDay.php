<?php namespace Childcare\AttendanceDays;

use Childcare\BaseModel;

/**
 * Class AttendanceDay
 * @package app\Childcare\AttendanceDay
 */
class AttendanceDay extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'attendance_days';

    /**
     * @var array
     */
    protected $fillable = ['child_id', 'day'];

    /**
     * @var array
     */
    public static $rules = [
        'child_id' => 'required',
        'day' => 'required|in:Mon,Tue,Wed,Thu,Fri,Sat,Sun'
    ];

    /**
     * @param $childId
     * @param $days
     */
    public static function setDays($childId, $days)
    {
        self::whereChildId($childId)->delete();

        foreach ($days as $day)
        {
            self::create([
                'child_id' => $childId,
                'day' => $day
            ]);
        }
    }
}