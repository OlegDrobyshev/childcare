<?php namespace Childcare\Attendance;

use Carbon\Carbon;
use Childcare\BaseModel;
use Childcare\Families\Family;
use DB;

/**
 * Class Attendance
 * @package app\Childcare\Attendance
 */
class Attendance extends BaseModel
{

    /**
     * @var string
     */
    public $table = 'attendance';

    /**
     * @var array
     */
    protected $fillable = ['teacher_id', 'child_id', 'date'];

    /**
     * @param $start
     * @param $end
     * @return array
     */
    public function scheduledAttendance($start, $end)
    {
        $start = date('Y-m-d 00:00:00', strtotime($start));
        $end = date('Y-m-d 00:00:00', strtotime($end));

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end);

        $items = [];

        while ($start->lte($end))
        {
            list($title, $groups) = $this->_getDayScheduledAttendance($start->format('Y-m-d'));

            if ($groups)
            {
                $date = $start->format('Y-m-d H:i:s');

                $items[] = [
                    'title' => $title,
                    'start' => $date,
                    'end' => $date,
                    'allDay' => true,
                    'color' => 'inherit',
                    'textColor' => '#26b4d7',
                    'borderColor' => 'transparent',
                    'overlap' => false,
                    'url'     => '/attendance/day/' . $start->format('Y-m-d')
                ];
            }

            $start->addDay();
        }

        return $items;
    }

    /**
     * @param $day
     * @return array
     */
    public static function day($day)
    {
        $dayOfWeek = date('D', strtotime($day));

        $children = DB::select("
            SELECT
              DISTINCT(c.id),
              c.name,
              c.family_id as family_id,
              c.weekly_rate,
              c.discount,
              if (c.full_day, 'Full Day', 'Half Day') as duration,
              f.last_name,
              if (a.date = '{$day}', 1, 0) as attended,
              g.id as group_id,
              a.teacher_id,
              a.drop_off,
              a.pick_up
            FROM children c
            LEFT JOIN attendance a ON c.id = a.child_id AND a.date = '{$day}' OR a.date IS NULL
            LEFT JOIN attendance_days as ad ON c.id = ad.child_id
            LEFT JOIN groups g ON c.group_id = g.id
            LEFT JOIN families f ON c.family_id = f.id
            WHERE ad.day = '{$dayOfWeek}' AND a.date IS NULL
            OR ad.day = '{$dayOfWeek}' AND a.date = '{$day}'
            OR a.date = '{$day}'
            OR ad.day = '{$dayOfWeek}'
            ORDER BY name ASC
        ");

        $data = [];

        foreach ($children as $child)
        {
            $childData = [
                'id' => $child->id,
                'name' => $child->name,
                'last_name' => $child->last_name,
                'family_id' => $child->family_id,
                'duration' => $child->duration,
                'siblings' => (Family::totalChildren($child->family_id) > 1) ? 'Yes' : 'No',
                'attended' => $child->attended,
                'teacher_id' => $child->teacher_id,
                'drop_off' => $child->drop_off,
                'pick_up' => $child->pick_up,
                'weekly_rate' => $child->weekly_rate,
                'discount' => $child->discount
            ];

            $data[$child->group_id][] = $childData;
        }

        return [array_keys($data), $data];
    }

    /**
     * @param $data
     * @return static
     */
    public static function updateForDay($data)
    {
        if (isset($data['status']) && ! $data['status'])
        {
            return self::whereChildId($data['child_id'])
                ->where('date', $data['date'])
                ->delete();
        }

        $attendance = self::firstOrCreate([
            'child_id' => $data['child_id'],
            'date' => $data['date']
        ]);

        unset($data['child_id'], $data['date'], $data['status']);

        foreach ($data as $column => $value)
        {
            $attendance->{$column} = $value;
        }

        $attendance->save();

        return $attendance;
    }

    /**
     * @param $date
     * @return array
     */
    protected function _getDayScheduledAttendance($date)
    {
        $dayOfWeek = date('D', strtotime($date));

        $data = DB::select("
            SELECT
                g.name,
                COUNT(DISTINCT (c.id)) as total
            FROM children c
            LEFT JOIN attendance_days ad ON ad.child_id = c.id
            LEFT JOIN attendance a ON a.child_id = c.id
            LEFT JOIN groups g ON g.id = c.group_id

            WHERE ad.day = '{$dayOfWeek}'
            OR a.date = '{$date}'

            GROUP BY g.id
        ");

        $title = '';

        $byGroups = [];

        foreach ($data as $group)
        {
            $title .= $group->name . ' - ' . $group->total . "\n";

            $byGroups[] = [
                'group' => $group->name,
                'totalChildren' => $group->total
            ];
        }

        return [$title, $byGroups];
    }

    /**
     * @param $date
     * @param $childId
     * @return static
     */
    public static function addChildToDate($date, $childId)
    {
        return self::firstOrCreate([
            'date' => $date,
            'child_id' => $childId
        ]);
    }

}