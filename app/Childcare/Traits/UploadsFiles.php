<?php namespace Childcare\Traits;

use Illuminate\Support\Facades\Storage;

/**
 * trait UploadsFiles
 * @package App\Traits
 */
trait UploadsFiles
{

    /**
     * @param $file
     * @param $path
     * @param null $extension
     * @return bool|string
     */
    public function move($file, $path, $extension = null)
    {
        $this->_prepareDirectory($path);

        $imgPath = $this->_getNewPath($file, $path, $extension);

        $moved = $this->_moveFile($file, $imgPath);

        if ($moved)
        {
            return '/storage/' . $imgPath;
        }

        return false;
    }

    /**
     * @param $path
     * @param string $storage
     */
    protected function _prepareDirectory($path, $storage = 'local')
    {
        if ( ! Storage::disk($storage)->exists($path))
        {
            Storage::disk($storage)->makeDirectory($path);
        }
    }

    /**
     * @param $file
     * @param $path
     * @param $extension
     * @return string
     */
    protected function _getNewPath($file, $path, $extension)
    {
        if ( ! $extension)
        {
            $extension = is_string($file)
                ? pathinfo($file, PATHINFO_EXTENSION)
                : $file->guessExtension();
        }

        return $path . '/' .sha1(microtime()) . '.' . $extension;
    }

    /**
     * @param $file
     * @param $path
     * @param string $storage
     * @return mixed
     */
    protected function _moveFile($file, $path, $storage = 'local')
    {
        if (is_object($file) && (get_class($file) == 'Intervention\Image\Image'))
        {
            $fileContent = $file->__toString();
        }
        else
        {
            $fileContent = file_get_contents($file);
        }

        return Storage::disk($storage)->put($path, $fileContent);
    }
}