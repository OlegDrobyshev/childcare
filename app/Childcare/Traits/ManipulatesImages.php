<?php namespace Childcare\Traits;

use Intervention\Image\Facades\Image;

/**
 * trait ManipulatesImages
 * @package App\Traits
 */
trait ManipulatesImages
{

    /**
     * @var
     */
    protected $_originalImg;

    /**
     * @var
     */
    protected $_imageInstance;

    /**
     * @param $imgFile
     * @param array $actions
     * @param string $format
     * @param int $quality
     * @return mixed
     */
    public function optimizeImage($imgFile, array $actions, $format = 'jpeg', $quality = 100)
    {
        $this->_initImage($imgFile);

        $this->_applyImageModifications($actions);

        return $this->_imageInstance
            ->encode($format, $quality);
    }

    /**
     * @param $imgFile
     */
    protected function _initImage($imgFile)
    {
        $this->_originalImg = $imgFile;

        $this->_imageInstance = Image::make($this->_originalImg);
    }

    /**
     * @param array $actions
     */
    protected function _applyImageModifications($actions)
    {
        foreach ($actions as $action => $params)
        {
            $method = '_do' . ucfirst($action);

            if (method_exists($this, $method))
            {
                call_user_func_array([$this, $method], $params);
            }
        }
    }

    /**
     * @param $size
     * @param bool $aspectRatio
     */
    protected function _doResizeByBiggerSize($size, $aspectRatio = true)
    {
        if ($this->_imageInstance->width() >= $this->_imageInstance->height())
        {
            $this->_imageInstance->resize($size, null, function ($constraint) use ($aspectRatio)
            {
                if ($aspectRatio) $constraint->aspectRatio();
            });
        }
        else
        {
            $this->_imageInstance->resize(null, $size, function ($constraint) use ($aspectRatio)
            {
                if ($aspectRatio) $constraint->aspectRatio();
            });
        }
    }

    /**
     * @param $width
     * @param $height
     */
    protected function _doResize($width, $height)
    {
        $this->_imageInstance->fit($width, $height);
    }
}