<?php namespace Childcare\Groups;

use Childcare\BaseModel;

/**
 * Class Group
 * @package app\Childcare\Groups
 */
class Group extends BaseModel
{

    /**
     * @var string
     */
    protected $table = 'groups';

    /**
     * @var array
     */
    public $fillable = ['name'];

    /**
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('Childcare\Children\Child', 'group_id', 'id');
    }

    /**
     * @return int
     */
    public function getPopulationAttribute()
    {
        return count($this->children);
    }

    /**
     * @return array
     */
    public function getForSelect()
    {
        $groups = $this->all();

        $data = [0 => 'Group'];

        foreach ($groups as $group)
        {
            $data[$group->id] = $group->name;
        }

        return $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getById($id)
    {
        $method = is_array($id) ? 'whereIn' : 'where';

        return self::$method('id', $id)->get();
    }
}