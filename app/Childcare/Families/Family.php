<?php namespace Childcare\Families;

use Childcare\BaseModel;

/**
 * Class Family
 */
class Family extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'families';

    /**
     * @var array
     */
    protected $fillable = ['last_name'];

    /**
     * @var array
     */
    public static $rules = [
        'last_name' => 'required'
    ];

    const PAGINATION_PER_PAGE = 5;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parents()
    {
        return $this->hasMany('Childcare\Parents\Parents', 'family_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('Childcare\Children\Child', 'family_id', 'id');
    }

    /**
     * @return int
     */
    public function getPopulationAttribute()
    {
        return count($this->parents) + count($this->children);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this
            ->orderBy('last_name')
            ->paginate(self::PAGINATION_PER_PAGE);
    }

    /**
     * @param $q
     * @return mixed
     */
    public function search($q)
    {
        return $this
            ->where('last_name', 'like', '%' . $q . '%')
            ->orderBy('last_name')
            ->paginate(self::PAGINATION_PER_PAGE);
    }

    /**
     * @param $familyId
     * @return int
     */
    public static function totalChildren($familyId)
    {
        $family = self::find($familyId);

        if ($family)
        {
            return $family->children()->count();
        }

        return 0;
    }

    /**
     * @param $string
     * @return array
     */
    public function searchByLastName($string)
    {
        $families = $this
            ->where('last_name', 'LIKE', '%' . $string . '%')
            ->orderBy('last_name')
            ->get();

        $data = [];

        foreach ($families as $family)
        {
            $data[] = [
                'id' => $family->id,
                'value' => ucfirst($family->last_name)
            ];
        }

        return $data;
    }

}