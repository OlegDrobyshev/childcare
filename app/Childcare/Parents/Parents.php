<?php namespace Childcare\Parents;

use Childcare\BaseModel;

/**
 * Class Parents
 * @package Children\Parents
 */
class Parents extends BaseModel
{

    /**
     * @var string
     */
    protected $table = 'parents';

    /**
     * @var array
     */
    protected $fillable = ['family_id', 'name', 'email', 'phone', 'address'];

    /**
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'address' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function family()
    {
        return $this->belongsTo('Childcare\Families\Family', 'family_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->family->last_name) . ' ' . ucfirst($this->name);
    }
}