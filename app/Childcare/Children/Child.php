<?php namespace Childcare\Children;

use Childcare\AttendanceDays\AttendanceDay;
use Carbon\Carbon;
use Childcare\BaseModel;
use Childcare\Traits\ManipulatesImages;
use Childcare\Traits\UploadsFiles;
use Illuminate\Support\Facades\Storage;

/**
 * Class Child
 */
class Child extends BaseModel
{
    use UploadsFiles, ManipulatesImages;

    /**
     * @var string
     */
    protected $table = 'children';

    /**
     * @var array
     */
    protected $fillable = [
        'family_id', 'group_id', 'name', 'gender', 'dob',
        'photo', 'full_day', 'allergies', 'notes', 'weekly_rate', 'discount'
    ];

    /**
     * @var array
     */
    public static $rules = [
        'family_id' => 'required',
        'group_id' => 'required',
        'name' => 'required',
        'dob' => 'required',
        'gender' => 'required|in:male,female',
        'weekly_rate' => 'required|integer',
        'discount' => 'integer'
//        'photo' => 'mimes:jpeg,png|max:1024'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function family()
    {
        return $this->belongsTo('Childcare\Families\Family', 'family_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('Childcare\Groups\Group', 'group_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function days()
    {
        return $this->hasMany('Childcare\AttendanceDays\AttendanceDay', 'child_id', 'id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->name) . ' ' . ucfirst($this->family->last_name);
    }

    /**
     * @return int
     */
    public function getAgeAttribute()
    {
        return Carbon::createFromTimestamp(strtotime($this->dob))->diff(
            Carbon::now())->format('%y years, %m months'
        );
    }

    /**
     * @return string
     */
    public function getDobHumanAttribute()
    {
        return Carbon::createFromTimestamp(strtotime($this->dob))->format('M j, Y');
    }

    /**
     * @return string
     */
    public function getDurationAttribute()
    {
        return $this->full_day ? 'Full Day' : 'Half Day';
    }

    /**
     * @return mixed
     */
    public function getGroupNameAttribute()
    {
        return $this->group ? $this->group->name : '---';
    }

    /**
     * @return mixed
     */
    public function getRateAttribute()
    {
        if ($this->discount)
        {
            return $this->weekly_rate - (($this->weekly_rate / 100) * $this->discount);
        }

        return $this->weekly_rate;
    }

    /**
     * @return bool
     */
    public function deletePhoto()
    {
        if ($this->photo)
        {
            return Storage::delete(str_replace('storage', '', $this->photo));
        }

        return true;
    }

    /**
     * @param $data
     * @return static
     */
    public function createChild($data)
    {
        if ($data['photoFile'])
        {
            $actions = [
                'resize' => [500, 400]
            ];

            $image = $this->optimizeImage($data['photoFile'], $actions, 'jpeg', 65);

            $path = 'public/children/photos';

            $data['photo'] = $this->move($image, $path, 'jpeg') ?: '';
        }

        $attendances = $data['attendance'];

        unset($data['photoFile'], $data['attendance']);

        $child = Child::create($data);

        if ( ! $child->getErrors() && $attendances)
        {
            AttendanceDay::setDays($child->id, $attendances);
        }

        return $child;
    }

    /**
     * @param $data
     * @return \Illuminate\Support\Collection|null|static
     */
    public function updateChild($data)
    {
        $child = $this->find($data['child_id']);

        if ($data['photoFile'])
        {
            $child->deletePhoto();

            $actions = [
                'resize' => [500, 400]
            ];

            $image = $this->optimizeImage($data['photoFile'], $actions, 'jpeg', 65);

            $path = 'public/children/photos';

            $data['photo'] = $this->move($image, $path, 'jpeg') ?: '';
        }

        if ($data['attendance'])
        {
            AttendanceDay::setDays($data['child_id'], $data['attendance']);
        }

        unset($data['child_id'], $data['photoFile'], $data['attendance']);

        foreach ($data as $column => $value)
        {
            $child->{$column} = $value;
        }

        $child->save();

        return $child;
    }

    /**
     * @param $string
     * @return array
     */
    public function searchByName($string)
    {
        $children = $this
            ->where('name', 'LIKE', '%' . $string . '%')
            ->orderBy('name')
            ->get();

        $data = [];

        foreach ($children as $child)
        {
            $data[] = [
                'id' => $child->id,
                'value' => $child->full_name
            ];
        }

        return $data;
    }

}