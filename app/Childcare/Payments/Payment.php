<?php namespace App\Childcare\Payments;

use Childcare\BaseModel;

/**
 * Class Payment
 * @package app\Childcare\Payments
 */
class Payment extends BaseModel
{

    /**
     *
     */
    const PAGINATION_PER_PAGE = 30;

    /**
     * @var string
     */
    protected $table = 'payments';

    /**
     * @var array
     */
    protected $fillable = ['family_id', 'type', 'amount', 'notes'];

    /**
     * @var array
     */
    public static $rules = [
        'family_id' => 'required|integer|exists:families,id',
        'type' => 'in:cash,check',
        'amount' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function family()
    {
        return $this->belongsTo('Childcare\Families\Family', 'family_id', 'id');
    }

    /**
     * @return string
     */
    public function getFamilyAttribute()
    {
        return ucfirst($this->family()->get()[0]->last_name);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this
            ->orderBy('id', 'desc')
            ->paginate(self::PAGINATION_PER_PAGE);
    }

    /**
     * @return mixed
     */
    public static function totalBilled()
    {
        return self::sum('amount');
    }
}