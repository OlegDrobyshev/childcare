<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('children', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('family_id')->unsigned();
            $table->integer('group_id')->unsigned()->nullable();
            $table->string('name');
            $table->enum('gender', ['male', 'female'])->index();
            $table->timestamp('dob');
            $table->string('photo');
            $table->boolean('full_day')->index();
            $table->text('allergies');
            $table->text('notes');
            $table->integer('weekly_rate')->nullable();
            $table->integer('discount')->nullable();
			$table->timestamps();

            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('children');
	}

}
