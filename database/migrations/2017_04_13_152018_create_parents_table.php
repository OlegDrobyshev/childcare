<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parents', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('family_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
			$table->timestamps();

            $table->foreign('family_id')->references('id')->on('families');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('parents');
	}

}
