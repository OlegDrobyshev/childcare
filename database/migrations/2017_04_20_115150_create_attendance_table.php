<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attendance', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('teacher_id')->unsigned();
            $table->integer('child_id')->unsigned();
            $table->date('date')->index();
            $table->string('pick_up');
            $table->string('drop_off');
			$table->timestamps();

            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('attendance');
	}

}
