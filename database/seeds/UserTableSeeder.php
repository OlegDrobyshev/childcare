<?php

use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder
 */
class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->truncate();

        \App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'type' => 'admin'
        ]);

        \App\User::create([
            'name' => 'Susan',
            'email' => 'teacher@teacher.com',
            'password' => Hash::make('admin'),
            'type' => 'teacher'
        ]);
    }

}