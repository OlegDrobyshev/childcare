<?php

use Childcare\Groups\Group;
use Illuminate\Database\Seeder;

/**
 * Class GroupsTableSeeder
 * @package database\seeds
 */
class GroupsTableSeeder extends Seeder
{

    public function run()
    {
        $groupNames = ['Owl', 'Fox', 'Raccoon', 'Moose', 'Bear'];

        foreach ($groupNames as $groupName)
        {
            Group::create([
                'name' => $groupName
            ]);
        }
    }

}