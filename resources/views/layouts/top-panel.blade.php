<!-- top panel -->
<div class='site_top_panel wave slider'>

    <!-- canvas -->
    <div class='top_half_sin_wrapper'>
        <canvas class='top_half_sin' data-bg-color='#ffffff' data-line-color='#ffffff'></canvas>
    </div>
    <!-- / canvas -->

    <div class='container'>
        <div class='row_text_search'>
            <div id='top_panel_text'><a href="tel:0-000-000-00000"><i class="fa fa-phone-square"></i> 0-000-000-00000 </a>
                <a href="mailto:mail@mail.com"> <i class="fa fa-envelope-o"></i>mail@mail.com</a>
            </div>
        </div>
        <div id='top_panel_links'>
            @if (Auth::check())
                <a href="/auth/logout"><i class='fa fa-sign-out'></i> logout</a>
            @else
                <a href="/auth/login"><i class='fa fa-user'></i> sign in</a>
            @endif
        </div>
        <div class="site_top_panel_toggle"></div>
    </div>
</div>
<!-- / top panel -->