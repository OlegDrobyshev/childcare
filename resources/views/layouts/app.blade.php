<!DOCTYPE html>

<html lang="en-US">
    <head>
        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="/assets/img/cropped-favicon-192x192.png" />

        <title>Kiddy</title>

        {!! Html::style('/assets/css/font-awesome.css') !!}
        {!! Html::style('/assets/css/jquery.fancybox.css') !!}
        {!! Html::style('/assets/css/select2.css') !!}
        {!! Html::style('/assets/css/animate.css') !!}
        {!! Html::style('/assets/css/main.css') !!}
        {!! Html::style('/assets/css/shop.css') !!}

        {!! Html::script('/assets/js/jquery.js') !!}
        {!! Html::script('/assets/js/jquery-migrate.min.js') !!}

        <link rel="icon" href="/assets/img/cropped-favicon-32x32.png" sizes="32x32" />
        <link rel="icon" href="/assets/img/cropped-favicon-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="/assets/img/cropped-favicon-180x180.png">
        <meta name="msapplication-TileImage" content="/assets/img/cropped-favicon-270x270.png">
    </head>

    <body class="wide wave-style">
        <div class="page">

            @include('layouts.top-panel')

            <!-- header container -->
            <div class='header_cont'>

                <div class='header_mask'>
                    <div class='header_pattern'></div>
                    <div class='header_img'></div>
                </div>

                <header class='site_header logo-in-menu' data-menu-after="3">
                    <div class="header_box">

                        <div class="container">
                            <!-- logo -->
                            <div class="header_logo_part with_border" role="banner">
                                <a class="logo" href="/">
                                    <img src='/assets/img/logo1.png' data-at2x='/assets/img/logo1.png' alt />
                                </a>
                            </div>
                            <!-- / logo -->

                            @include('layouts.menu')
                        </div>
                    </div>
                </header>
                <!-- #masthead -->
            </div>
            <!-- / header container -->

            @yield('breadcrumbs')

            <!-- main container -->
            <div id="main" class="site-main">

                @yield('content')

            </div>
            <!-- #main -->

            <!-- footer -->
            <div class='footer_wrapper_copyright'>
                <!-- canvas -->
                <div class='half_sin_wrapper'>
                    <canvas class='half_sin' data-bg-color='23,108,129' data-line-color='23,108,129'></canvas>
                </div>
                <!-- / canvas -->

                <!-- copyright -->
                <div class='copyrights_area'>
                    <div class='half_sin_wrapper'>
                        <canvas class='footer_half_sin' data-bg-color='14,64,77' data-line-color='14,64,77'></canvas>
                    </div>
                    <div class='container'>
                        <div class='copyrights_container'>
                            <div class='copyrights'>Little Tree Huggers &copy; {{ date('Y') }} All rights reserved</div>
                            <div class='copyrights_panel'>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- / copyright -->
            </div>
            <!-- / footer -->

        </div>
        <!-- #page -->

        <div class='scroll_top animated'></div>

        {!! Html::script('/assets/js/retina_1.3.0.js') !!}
        {!! Html::script('/assets/js/modernizr.js') !!}
        {!! Html::script('/assets/js/owl.carousel.js') !!}
        {!! Html::script('/assets/js/TweenMax.min.js') !!}
        {!! Html::script('/assets/js/jquery.isotope.min.js') !!}
        {!! Html::script('/assets/js/jquery.fancybox.js') !!}
        {!! Html::script('/assets/js/select2.min.js') !!}
        {!! Html::script('/assets/js/wow.min.js') !!}
        {!! Html::script('/assets/js/jquery.validate.min.js') !!}
        {!! Html::script('/assets/js/jquery.form.min.js') !!}
        {!! Html::script('/assets/js/scripts.js') !!}
        {!! Html::script('/assets/js/jquery.tweet.js') !!}

            @yield("scripts")

    </body>

</html>
