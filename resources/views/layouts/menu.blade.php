@if (Auth::user()->isAdmin())

<!-- menu -->
<div class="header_nav_part">
    <nav class="main-nav-container a-center">
        <div class="mobile_menu_header">
            <i class="mobile_menu_switcher"><span></span><span></span><span></span></i>
        </div>
        <ul id="menu-main-menu" class="main-menu menu-bees">
            <!-- menu item -->
            <li class="menu-item bees-start">
                <a href="/">
                    <div class="bees bees-start"><span></span>
                        <div class="line-one"></div>
                        <div class="line-two"></div>
                    </div><i class="fa fa-home" aria-hidden="true"></i>
                    <div class="canvas_wrapper">
                        <canvas class="menu_dashed"></canvas>
                    </div>
                </a>
            </li>
            <!-- / menu item -->

            <!-- menu item -->
            <li class="menu-item">
                <a href="/families">
                    Families<div class="canvas_wrapper"><canvas class="menu_dashed"></canvas></div>
                </a>
            </li>
            <!-- / menu item -->

            <!-- menu item -->
            <li class="menu-item menu-item-has-children">
                <a href="/payments">
                    Payments<div class="canvas_wrapper"><canvas class="menu_dashed"></canvas></div>
                </a>
            </li>
            <!-- / menu item -->

            <!-- menu item -->
            <li class="menu-item right">
                <a href="/groups">
                    Groups<div class="canvas_wrapper"><canvas class="menu_dashed"></canvas></div>
                </a>
            </li>
            <!-- / menu item -->

            <!-- menu item -->
            <li class="menu-item right">
                <a href="/attendance">
                    Schedule<div class="canvas_wrapper"><canvas class="menu_dashed"></canvas></div>
                </a>
            </li>
            <!-- / menu item -->

            <!-- menu item -->
            <li class="menu-item right bees-end">
                @if (Auth::check())
                    <a href="/auth/logout">
                        <div class="bees bees-end"><span></span>
                            <div class="line-one"></div>
                            <div class="line-two"></div>
                        </div>
                        {{--<i class='fa fa-sign-out'></i>--}}Log Out
                        <div class="canvas_wrapper">
                            <canvas class="menu_dashed"></canvas>
                        </div>
                    </a>
                @else
                    <a href="/auth/login">
                        <div class="bees bees-end"><span></span>
                            <div class="line-one"></div>
                            <div class="line-two"></div>
                        </div>
                        <i class='fa fa-user'></i>
                        <div class="canvas_wrapper">
                            <canvas class="menu_dashed"></canvas>
                        </div>
                    </a>
                @endif
            </li>
            <!-- / menu item -->

        </ul>
    </nav>
</div>
<!-- / menu -->

@endif