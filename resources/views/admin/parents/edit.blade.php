@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Parent: {{ $parent->full_name }}</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/families/" >Families</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/family/{{ $parent->family_id }}" >The {{ $parent->family->last_name }}s</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">{{ $parent->full_name }}</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main>

            <!-- Edit form -->
            <div class='grid_row clearfix'>
                <div class='grid_col grid_col_3'>
                    <div class='ce clearfix'>&nbsp;</div>
                </div>

                <div class='grid_col grid_col_6'>
                    <div class='ce clearfix'>

                        <h1 class="ce_title text-center">{{ $parent->full_name }}</h1>

                        @if (Session::has('message'))
                            {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
                        @endif

                        {!! Form::open(['url' => '/parent/' . $parent->id . '/update']) !!}

                            <div class="form-input">
                                {!! Form::text('name', $parent->name, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Name'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::email('email', $parent->email, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'email'),
                                    'placeholder' => '* Email'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('phone', $parent->phone, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'phone'),
                                    'placeholder' => '* Phone'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('address', $parent->address, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'address'),
                                    'placeholder' => '* Address'
                                ]) !!}
                            </div>

                            <div class="form-input text-center">
                                <button type="submit" class="cws_button alt">
                                    <span class="cws_button_inner">Save</span>
                                </button>
                                <a href="/family/{{ $parent->family_id }}" style="margin: 30px 0 0 10px;">Cancel</a>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div><!-- / Edit form -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection