@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Groups : total {{ $groups->count() }}</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">Groups</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main style="overflow-x: scroll;">

            <!-- Toolbox -->
            <div class='grid_row clearfix'>

                <!-- Notifications -->
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix'>
                        @if (Session::has('message'))
                            {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
                        @endif
                    </div>
                </div><!-- / Notifications -->

                <div id="toolbox" class='grid_col grid_col_12' style="margin-left: 0;">
                    <div class='ce clearfix'>
                        <div class="cws_ce_content ce_accordion">

                            <!-- Create new -->
                            <div class="accordion_section">
                                <div class="accordion_title no-border" id="create-group-form">
                                    <i class="accordion_icon"></i><span class="v-align-top">Create New</span>
                                </div>
                                <div class="accordion_content" style="display: none;">
                                    {!! Form::open(['url' => 'groups/store']) !!}
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '* Group Name']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div><!-- / Create new -->

                        </div>
                    </div>
                </div>
            </div><!-- / Toolbox -->

            <!-- Table -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix' style="text-align: center;">

                        @if (count($groups))
                            <table class="table" style="margin: 0 auto; width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center nowrap">#</th>
                                        <th class="text-center nowrap">Last Name</th>
                                        <th class="text-center nowrap">Population</th>
                                        <th class="text-center nowrap">Created At</th>
                                        {{--<th class="text-center nowrap">Updated At</th>--}}
                                        {{--<th class="text-center nowrap">Actions</th>--}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($groups as $group)
                                        <tr>
                                            <td class="text-center">{{ $group->id }}</td>
                                            <td class="text-center">{{ $group->name }}</td>
                                            <td class="text-center">{{ $group->population }}</td>
                                            <td class="text-center">{{ $group->created_at->format('Y-m-d H:i') }}</td>
{{--                                            <td class="text-center">{{ $group->updated_at->format('Y-m-d H:i') }}</td>--}}
                                            {{--<td class="text-center">--}}
                                                {{--<a href="/family/{{ $group->id }}"><i class="fa fa-pencil" aria-hidden="true"></i> </a>&nbsp;&nbsp;--}}
                                                {{--<a href="/family/{{ $group->id }}/delete" class="delete-family" data-last_name="{{ $group->last_name }}"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>--}}
                                            {{--</td>--}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div><!-- / Table -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::script('/assets/pages/groups.js') !!}
    <script>
        Groups.init({
            "errorForm": "{{ Session::get('error-form', '') }}"
        });
    </script>
@endsection