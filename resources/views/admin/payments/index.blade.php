@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Payments</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">Payments</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main style="overflow-x: scroll;">

            <!-- Toolbox -->
            <div class='grid_row clearfix'>

                <!-- Notifications -->
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix'>
                        @if (Session::has('message'))
                            {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
                        @endif
                    </div>
                </div><!-- / Notifications -->

                <div id="toolbox" class='grid_col grid_col_12' style="margin-left: 0;">
                    <div class='ce clearfix'>
                        <div class="cws_ce_content ce_accordion">

                            <!-- Create new -->
                            <div class="accordion_section">
                                <div class="accordion_title no-border" id="create-payment-form">
                                    <i class="accordion_icon"></i><span class="v-align-top">Create New</span>
                                </div>
                                <div class="accordion_content" style="display: none;">
                                    {!! Form::open([
                                        'url' => 'payments/store',
                                        'id' => 'create-payment-form'
                                    ]) !!}

                                        <div class="form-input">
                                            {!! Form::text('family_id', null, [
                                                'class' => 'form-control ' . getErrorClass($errors, 'family_id'),
                                                'id' => 'find-family-ajax',
                                                'style' => 'width: 100%'
                                            ]) !!}
                                        </div>

                                        <div class="form-input">
                                            {!! Form::select('type', ['cash' => 'Cash', 'check' => 'Check'], null, [
                                                'class' => 'form-control ' . getErrorClass($errors, 'type'),
                                                'placeholder' => '* Type'
                                            ]) !!}
                                        </div>

                                        <div class="form-input">
                                            {!! Form::text('amount', null, [
                                                'class' => 'form-control ' . getErrorClass($errors, 'amount'),
                                                'placeholder' => '* Amount'
                                            ]) !!}
                                        </div>

                                        <div class="form-input">
                                            {!! Form::textarea('notes', null, [
                                                'class' => 'form-control ' . getErrorClass($errors, 'notes'),
                                                'placeholder' => 'Notes',
                                                'rows' => 3
                                            ]) !!}
                                        </div>

                                        <div class="form-input">
                                            {!! Form::submit('Create') !!}
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div><!-- / Create new -->

                        </div>
                    </div>
                </div>
            </div><!-- / Toolbox -->

            <!-- Table -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix' style="text-align: center;">

                        @if (count($payments))
                            <table class="table" style="margin: 0 auto; width: 100%;">
                                <thead>
                                    <tr>
                                        <th class="text-center nowrap">#</th>
                                        <th class="text-center nowrap">Family</th>
                                        <th class="text-center nowrap">Type</th>
                                        <th class="text-center nowrap">Amount</th>
                                        <th class="text-center nowrap">Pay Date</th>
                                        <th class="text-center nowrap">Notes</th>
                                        <th class="text-center nowrap">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($payments as $payment)
                                        <tr>
                                            <td class="text-center">{{ $payment->id }}</td>
                                            <td class="text-center">
                                                <a href="/family/{{ $payment->family_id }}">{{ $payment->family }}</a>
                                            </td>
                                            <td class="text-center">{{ $payment->type }}</td>
                                            <td class="text-center">$ {{ $payment->amount }}</td>
                                            <td class="text-center">{{ $payment->created_at->format('Y-m-d') }}</td>
                                            <td class="text-center">
                                                @if ($payment->notes)
                                                    {{ $payment->notes }}
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="/payment/{{ $payment->id }}/delete" class="delete-payment"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div><!-- / Table -->

            <!-- Pagination -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix' style="text-align: center;">
                        {!! $payments->render() !!}
                    </div>
                </div>
            </div><!-- / Pagination -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::script('/assets/pages/payments.js') !!}
    <script>
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        Payments.init( {
            "errorForm": "{{ Session::get('error-form', '') }}"
        } );
    </script>
@endsection
