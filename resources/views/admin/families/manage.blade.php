@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>The {{ ucfirst($family->last_name) }}s</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/families/" >Families</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">The {{ ucfirst($family->last_name) }}s</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main>
            <!-- Toolbox -->
            @include('admin.families.partials.family-toolbox')

            <!-- Members -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix'>

                        @if ( ! $parents->count() && ! $children->count())
                            <div style="height: 220px;"></div>
                        @endif

                        <!-- Children -->
                        @include('admin.families.partials.family-children-table')

                        <!-- Parents -->
                        @include('admin.families.partials.family-parents-table')

                    </div>
                </div>
            </div><!-- / Members -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::style('/assets/plugins/datepicker/datepicker.min.css') !!}

    {!! Html::script('/assets/plugins/datepicker/datepicker.min.js') !!}
    {!! Html::script('/assets/pages/family.js') !!}
    <script>
        Family.init({
            "errorForm": "{{ Session::get('error-form', '') }}"
        });
    </script>
@endsection