@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Families
                    @if (isset($searchString))
                        : "{{ $searchString }}", {{ $families->total() }} found
                        <a href='/families' style="color: #ff4c7c;">
                            <i class="fa fa-times"></i>
                        </a>
                    @else
                        : total {{ $families->total() }}
                    @endif
                </h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">Families</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main style="overflow-x: scroll;">

            <!-- Toolbox -->
            <div class='grid_row clearfix'>

                <!-- Notifications -->
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix'>
                        @if (Session::has('message'))
                            {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
                        @endif
                    </div>
                </div><!-- / Notifications -->

                <div class='grid_col grid_col_12' style="margin-left: 0;">
                    <div class='ce clearfix'>
                        <div class="cws_ce_content ce_accordion">

                            <!-- Create new -->
                            <div class="accordion_section">
                                <div class="accordion_title no-border">
                                    <i class="accordion_icon"></i><span class="v-align-top">Create New</span>
                                </div>
                                <div class="accordion_content" style="display: none;">
                                    {!! Form::open(['url' => 'families/store']) !!}
                                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => '* Last Name']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div><!-- / Create new -->

                            <!-- Search -->
                            <div class="accordion_section">
                                <div class="accordion_title no-border">
                                    <i class="accordion_icon"></i><span class="v-align-top">Search by Last Name</span>
                                </div>
                                <div class="accordion_content" style="display: none;">
                                    {!! Form::open(['url' => 'families/search', 'id' => 'search']) !!}
                                        {!! Form::text('q', null, ['class' => 'form-control', 'placeholder' => '* Last Name', 'required' => 'required']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div><!-- / Search -->

                        </div>
                    </div>
                </div>
            </div><!-- / Toolbox -->

            <!-- Table -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix' style="text-align: center;">

                        @if (count($families))
                            <table class="table" style="margin: 0 auto; width: 100%;">
                                <thead>
                                    <tr>
                                        {{--<th class="text-center nowrap">#</th>--}}
                                        <th class="text-center nowrap">Last Name</th>
                                        <th class="text-center nowrap">Population</th>
                                        {{--<th class="text-center nowrap">Created At</th>--}}
                                        {{--<th class="text-center nowrap">Updated At</th>--}}
                                        <th class="text-center nowrap">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($families as $family)
                                        <tr>
                                            {{--<td class="text-center">{{ $family->id }}</td>--}}
                                            <td class="text-center">{{ $family->last_name }}</td>
                                            <td class="text-center">{{ $family->population }}</td>
{{--                                            <td class="text-center">{{ $family->created_at->format('m-d-y g:i a') }}</td>--}}
{{--                                            <td class="text-center">{{ $family->updated_at->format('m-d-y g:i a') }}</td>--}}
                                            <td class="text-center">
                                                <a href="/family/{{ $family->id }}"><i class="fa fa-pencil" aria-hidden="true"></i> </a>&nbsp;&nbsp;
                                                <a href="/family/{{ $family->id }}/delete" class="delete-family" data-last_name="{{ $family->last_name }}"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div><!-- / Table -->

            <!-- Pagination -->
            <div class='grid_row clearfix no-padding'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix' style="text-align: center;">
                        {!! $families->render() !!}
                    </div>
                </div>
            </div><!-- / Pagination -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::script('/assets/pages/families.js') !!}
    <script>
        Families.init();
    </script>
@endsection