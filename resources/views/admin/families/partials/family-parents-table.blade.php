@if ($parents->count())

    <div class='grid_row no-padding clearfix'>
        <div class='grid_col grid_col_12'>
            <div class='ce clearfix'>
                <h3 class="ce_title">Parents</h3>
            </div>
        </div>
    </div>

    <div class="grid_row eq_cols" style="padding-bottom: 50px;">

        @foreach ($parents as $parent)

            <div class="grid_col grid_col_3 pricing_table_column">
                <div>
                    <div class="top_section">
                        <div class="title_section">{{ $parent->name }}</div>
                        <div class="encouragement">{{ $parent->family->last_name }}</div>
                        <div class="separate"></div>
                        <div class="price_section"></div>
                    </div>
                    <div class="desc_section">
                        <ul class="listing">
                            <li><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:{{ $parent->email }}">{{ $parent->email }}</a></li>
                            <li><i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="tel:{{ $parent->phone }}">{{ $parent->phone }}</a></li>
                            <li><i class="fa fa-map-marker"></i>&nbsp;&nbsp;{{ $parent->address }}</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="btn_section">
                        <a href="/parent/{{ $parent->id }}" class="cws_button">
                            Edit
                            <div class="hover-btn"></div>
                            <div class="button-shadow"></div>
                        </a>
                        <a href="/parent/{{ $parent->id }}/delete" class="delete-parent" data-name="{{ $parent->full_name }}" style="color: #FE5C5C; font-size: 30px; padding-left: 20px;">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
@endif