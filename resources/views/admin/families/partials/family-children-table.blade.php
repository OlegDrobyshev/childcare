@if ($children->count())

    <div class='grid_row no-padding clearfix'>
        <div class='grid_col grid_col_12'>
            <div class='ce clearfix'>
                <h3 class="ce_title">Children</h3>
            </div>
        </div>
    </div>

    <div class="grid_row eq_cols" style="padding-bottom: 50px;">

        @foreach ($children as $child)

            <div class="grid_col grid_col_3 pricing_table_column">
                <div>
                    <div class="top_section">
                        <div class="title_section">{{ $child->name }}</div>
                        <div class="encouragement">{{ $child->family->last_name }}</div>
                        <div class="separate"></div>
                        <div class="price_section">

                            @if ($child->photo)
                                <div class='media_part'>
                                    <div class='pic'>
                                        <img class="border aligncenter image-type " src='{{ $child->photo }}' />
                                        <div class='links_popup animate'>
                                            <div class='link_cont'>
                                                <div class='link'>
                                                    <a class='fancy' href='{{ $child->photo }}'><i class='fa fa-camera'></i></a>
                                                    <div class='link-item-bounce'></div>
                                                </div>
                                                <div class='link'>
                                                    <a href='/child/{{ $child->id }}'><i class='fa fa-pencil'></i></a>
                                                    <div class='link-item-bounce'></div>
                                                </div>
                                            </div>
                                            <div class='link-toggle-button'>
                                                <i class='fa fa-plus link-toggle-icon'></i>
                                            </div>
                                        </div>
                                        <div class='hover-effect'></div>
                                    </div>
                                </div>
                            @else
                                &nbsp;
                            @endif

                            <div class="price_container">
                                <div class="vova-test"></div>
                                <span class="main_price_part gh"></span>
                                <span class="price_details">
                                    <span class="fract_price_part">{{ $child->age }}</span>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="desc_section">
                        <ul class="listing">
                            <li><i class="fa fa-birthday-cake"></i>&nbsp;&nbsp;{{ $child->dobHuman }}</li>
                            <li><i class="fa fa-venus-mars"></i>&nbsp;&nbsp;{{ ucfirst($child->gender) }}</li>
                            <li><i class="fa fa-calendar-check-o"></i>&nbsp;
                                @foreach ($child->days as $key => $day)
                                    {{ substr($day->day, 0, 1) }}
                                    @if ($key < (count($child->days) - 1))
                                        &#8226;
                                    @endif
                                @endforeach
                            </li>
                            <li><i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ ucfirst($child->duration) }}</li>
                            <li><i class="fa fa-users"></i>&nbsp;&nbsp;{{ ucfirst($child->groupName) }}</li>
                            <li><i class="fa fa-money"></i>&nbsp;&nbsp;
                                @if ($child->discount)
                                    <span style="color: #35e27e">$ {{ $child->rate }}</span>
                                    <span style="font-size: 16px; color:#FE5C5C;">(-{{ $child->discount }}%)</span>
                                @else
                                    <span style="color: #3eb2f9">$ {{ $child->rate }}</span>
                                @endif
                            </li>
                            <li><i class="fa fa-flask"></i>&nbsp;&nbsp;Allergies
                                <p style="padding-top: 10px;">{{ $child->allergies ?: '---' }}</p>
                            </li>
                            <li>
                                <i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Notes
                                <p style="padding-top: 10px;">
                                    {{ $child->notes ?: '---' }}
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="btn_section">
                        <a href="/child/{{ $child->id }}" class="cws_button">
                            Edit
                            <div class="hover-btn"></div>
                            <div class="button-shadow"></div>
                        </a>
                        <a href="/child/{{ $child->id }}/delete" class="delete-parent" data-name="{{ $child->full_name }}" style="color: #FE5C5C; font-size: 30px; padding-left: 20px;">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
@endif