<div class='grid_row clearfix'>

    <!-- Notifications -->
    <div class='grid_col grid_col_12'>
        <div class='ce clearfix'>
            @if (Session::has('message'))
                {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
            @endif
        </div>
    </div><!-- / Notifications -->

    <div id="toolbox" class='grid_col grid_col_12' style="margin-left: 0;">
        <div class='ce clearfix'>
            <div class="cws_ce_content ce_toggle">

                <!-- Create parent -->
                <div class="accordion_section">
                    <div class="accordion_title no-border" id="add-parent-form">
                        <i class="accordion_icon"></i><span class="v-align-top">Add Parent</span>
                    </div>
                    <div class="accordion_content" style="display: none;">

                        {!! Form::open(['url' => 'parents/store']) !!}
                            {!! Form::hidden('family_id', $family->id) !!}

                            <div class="form-input">
                                {!! Form::text('name', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Name'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::email('email', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'email'),
                                    'placeholder' => '* Email'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('phone', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'phone'),
                                    'placeholder' => '* Phone'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('address', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'address'),
                                    'placeholder' => '* Address'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::submit('Create') !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div><!-- / Create parent -->

                <!-- Create child -->
                <div class="accordion_section">
                    <div class="accordion_title no-border" id="add-child-form">
                        <i class="accordion_icon"></i><span class="v-align-top">Add Child</span>
                    </div>
                    <div class="accordion_content" style="display: none;">
                        {!! Form::open([
                            'url' => 'children/store',
                            'enctype' => 'multipart/form-data',
                            'id' => 'edit-child-form'
                        ]) !!}
                            {!! Form::hidden('family_id', $family->id) !!}

                            <div class="form-input">
                                {!! Form::text('name', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Name'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::select('gender', ['male' => 'Male', 'female' => 'Female'], null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Gender'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('dob', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'dob'),
                                    'placeholder' => '* Birthday',
                                    'data-toggle' => 'datepicker'
                                ]) !!}
                            </div>

                            <div class="form-input" style="margin-top: 10px;">
                                {!! Form::file('photoFile', [
                                    'class' => 'form-control ' . getErrorClass($errors, 'photo'),
                                    'style' => '
                                        color: #aaa;
                                        padding: .5rem .75rem;
                                        line-height: 8px;
                                        font-size: 14px;
                                        cursor: pointer;
                                        border-radius: 8px;
                                        border: 1px solid rgba(0, 0, 0, .15);
                                    '
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::textarea('allergies', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'allergies'),
                                    'placeholder' => 'Allergies',
                                    'rows' => 3
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::textarea('notes', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'notes'),
                                    'placeholder' => 'Notes',
                                    'rows' => 3
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::select('group_id', $groups, null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'group_id'),
                                    'placeholder' => '* Group'
                                ]) !!}
                            </div>

                            <div class="form-input text-center" style="margin: 20px 0;">
                                <label for="Sun" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="Sun" type="checkbox" name="attendance[]" value="Sun"> Sun
                                </label>
                                <label for="mon" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="mon" type="checkbox" name="attendance[]" value="Mon"> Mon
                                </label>
                                <label for="tue" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="tue" type="checkbox" name="attendance[]" value="Tue"> Tue
                                </label>
                                <label for="wed" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="wed" type="checkbox" name="attendance[]" value="Wed"> Wed
                                </label>
                                <label for="thu" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="thu" type="checkbox" name="attendance[]" value="Thu"> Thu
                                </label>
                                <label for="fri" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="fri" type="checkbox" name="attendance[]" value="Fri"> Fri
                                </label>
                                <label for="Sat" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="Sat" type="checkbox" name="attendance[]" value="Sat"> Sat
                                </label>
                            </div>

                            <div class="form-input">
                                {!! Form::text('weekly_rate', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'weekly_rate'),
                                    'placeholder' => '* Weekly Rate'
                                ] ) !!}
                            </div>
                            <div class="form-input">
                                {!! Form::text('discount', null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'discount'),
                                    'placeholder' => 'Discount in %'
                                ] ) !!}
                            </div>

                            <div class="form-input text-center">
                                <label for="duration-full-day" class="checkbox" style="display: inline-block; margin-right: 30px;">
                                    <input type="radio" name="full_day" id="duration-full-day" value="1" checked /> Full Day
                                </label>
                                <label for="duration-half-day" class="checkbox" style="display: inline-block;">
                                    <input type="radio" name="full_day" id="duration-half-day" value="0" /> Half Day
                                </label>
                            </div>

                            <div class="form-input">
                                {!! Form::submit('Create') !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div><!-- / Create child -->

            </div>
        </div>
    </div>
</div><!-- / Toolbox -->