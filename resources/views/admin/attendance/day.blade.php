@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Schedule: {{ date('M j, Y', strtotime($day)) }}</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/attendance/" >Schedule</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">{{ date('M j, Y', strtotime($day)) }}</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <style>
        .select2-container {
            min-width: auto!important;
        }

        .select2-choice,
        .select2-drop.select2-drop-above.select2-drop-active,
        .select2-drop, .select2-drop-active {
            border-color: #fec20b !important;
        }

    </style>

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main>

            <!-- Edit form -->
            <div class='grid_row clearfix'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix text-center' style="overflow-x: scroll;">



                        <div class="cws_callout" style="padding-top: 5px; border-bottom: 3px solid #fec20b;">
                            <div class="content_section">
                                <div class="callout_title"></div>
                                <div class="separate"></div>
                                <div class="callout_text" style="overflow: hidden;">

                                    <div class='grid_col grid_col_4 text-left'>
                                        <div class='ce clearfix'>
                                            {!! Form::input('child_id', null, null, [
                                                'id' => 'find-child-ajax',
                                                'style' => 'margin:0;width:150px;float:left;'
                                            ]) !!}
                                            &nbsp;
                                            <a href="/attendance" id="add-child-btn" class="cws_button small" style="margin: 0;padding: 10px 25px;position: relative; top: 2px;">
                                                + <i class="fa fa-user"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class='grid_col grid_col_4 text-center'>
                                        <div class='ce clearfix'>
                                            {!! Form::select('go-to-group', $groupsList, null, [
                                                'style' => 'margin:0;width:100%;'
                                            ]) !!}
                                        </div>
                                    </div>

                                    <div class='grid_col grid_col_4 text-right'>
                                        <div class='ce clearfix'>
                                            <a href="/attendance/day/{{ date('Y-m-d', strtotime('- 1 day ' . $day)) }}" class="cws_button small">< Prev</a>
                                            <a href="/attendance" class="cws_button small" style="margin:0;">
                                                <i class="fa fa-calendar"></i>
                                            </a>
                                            <a href="/attendance/day/{{ date('Y-m-d', strtotime('+ 1 day' . $day)) }}" class="cws_button small"  style="margin:0;">Next ></a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        @if ( ! $groups->count())

                            <div class="ce_title" style="height: 200px; font-size: 32px;">Nothing Scheduled Yet</div>

                        @else
                            @foreach ($groups as $key => $group)

                                <h2 id="{{ $group->name }}-group" class="ce_title">{{ $group->name }}</h2>

                                <table class="table attendance-table" style="margin: 0 auto; width: 100%;">
                                    <thead>
                                        <tr>
                                            {{--<th class="text-center nowrap">#</th>--}}
                                            <th class="text-center nowrap">Name</th>
                                            <th class="text-center nowrap">Last Name</th>
                                            <th class="text-center nowrap">Rate</th>
                                            <th class="text-center nowrap">Duration</th>
                                            <th class="text-center nowrap">Siblings</th>
                                            <th class="text-center nowrap">Attended</th>
                                            <th class="text-center nowrap">Teacher</th>
                                            <th class="text-center nowrap">Drop Off</th>
                                            <th class="text-center nowrap">Pick Up</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($children[$group->id] as $child)

                                            <tr data-child-id="{{ $child['id'] }}">
                                                {{--<td class="text-center">{{ $child['id']}}</td>--}}
                                                <td class="text-center">
                                                    <a href="/child/{{ $child['id']}}">{{ $child['name']}}</a>
                                                </td>
                                                <td class="text-center">
                                                    <a href="/family/{{ $child['family_id'] }}">{{ ucfirst($child['last_name']) }}</a>
                                                </td>
                                                <td class="text-center">
                                                    @if ($child['discount'])
                                                        <span style="color:#35e27e;">$ {{ $child['weekly_rate'] - (($child['weekly_rate'] / 100) * $child['discount']) }}</span> <br />
                                                        <span style="font-size: 16px; color:#FE5C5C; ">-{{ $child['discount'] }}%</span>
                                                    @else
                                                        $ {{ $child['weekly_rate'] }}
                                                    @endif
                                                </td>
                                                <td class="text-center">{{ $child['duration'] }}</td>
                                                <td class="text-center">{{ $child['siblings'] }}</td>
                                                <td class="text-center">
                                                    @if ($child['attended'])
                                                        {!! Form::checkbox('attended', 1, ['checked' => 'checked']) !!}
                                                    @else
                                                        {!! Form::checkbox('attended', 0) !!}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($child['attended'])
                                                        {!! Form::select('teacher_id', $teachers, $child['teacher_id'], [
                                                            'class' => 'select'
                                                        ]) !!}
                                                    @else
                                                        {!! Form::select('teacher_id', $teachers, null, [
                                                            'class' => 'select',
                                                            'disabled' => true
                                                        ]) !!}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($child['attended'])
                                                        {!! Form::select('drop_off', [
                                                            'Mother' => 'Mother',
                                                            'Father' => 'Father',
                                                            'Grandmother' => 'Grandmother',
                                                            'Grandfather' => 'Grandfather',
                                                            'Other' => 'Other'
                                                        ], $child['drop_off'], [
                                                            'class' => 'select'
                                                        ]) !!}
                                                    @else
                                                        {!! Form::select('drop_off', [
                                                            'Mother' => 'Mother',
                                                            'Father' => 'Father',
                                                            'Grandmother' => 'Grandmother',
                                                            'Grandfather' => 'Grandfather',
                                                            'Other' => 'Other'
                                                        ], $child['drop_off'], [
                                                            'class' => 'select',
                                                            'disabled' => true
                                                        ]) !!}
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($child['attended'])
                                                        {!! Form::select('pick_up', [
                                                            'Mother' => 'Mother',
                                                            'Father' => 'Father',
                                                            'Grandmother' => 'Grandmother',
                                                            'Grandfather' => 'Grandfather',
                                                            'Other' => 'Other'
                                                        ], $child['pick_up'], [
                                                            'class' => 'select'
                                                        ]) !!}
                                                    @else
                                                        {!! Form::select('pick_up', [
                                                            'Mother' => 'Mother',
                                                            'Father' => 'Father',
                                                            'Grandmother' => 'Grandmother',
                                                            'Grandfather' => 'Grandfather',
                                                            'Other' => 'Other'
                                                        ], $child['pick_up'], [
                                                            'class' => 'select',
                                                            'disabled' => true
                                                        ]) !!}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::script('/assets/pages/day.js') !!}
    <script>
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        Day.init({
            "day": "{{ $day }}",
            "addedChildId": "{{ Session::get('added-child-id', 0) }}"
        });
    </script>
@endsection