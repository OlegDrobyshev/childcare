@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'><h1>Schedule</h1></div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">Schedule</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content" style="padding-top: 20px;">
        <div class='left-pattern pattern pattern-2'></div>

        <main>
            <div class='grid_row clearfix'>
                <div class='grid_col grid_col_12'>
                    <div class='ce clearfix'>

                        {!! $calendar->calendar() !!}

                    </div>
                </div>
            </div>
        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    {!! $calendar->script() !!}
@endsection