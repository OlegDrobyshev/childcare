@extends('layouts.app')

@section('content')

    <div class="page_content">

        <div class='left-pattern pattern pattern-2'></div>

        <main style="padding-bottom: 80px;">
            <!-- content section -->
            <div class="grid_row clearfix">
                <h3 class="ce_title">Achievements</h3>

                <!-- callout -->
                <div class="grid_col grid_col_4">
                    <div class="ce clearfix">
                        <div class="cws_callout">
                            <div class="content_section">
                                <div class="separate"></div>
                                <div class="callout_title">
                                    <div class="bees bees-end"><span></span></div>
                                        {{ $totalFamilies }}
                                        @if ($totalFamilies > 1) Families @else Family @endif
                                    </div>
                                    <div class="callout_text">
                                </div>
                            </div>
                            <div class="button_section"><a href="/families" class="cws_button xlarge">View<div class="button-shadow"></div></a></div>
                        </div>
                    </div>
                </div>
                <!-- / callout -->
                <!-- callout -->
                <div class="grid_col grid_col_4">
                    <div class="ce clearfix">
                        <div class="cws_callout">
                            <div class="content_section">
                                <div class="separate"></div>
                                <div class="callout_title">
                                    <div class="bees bees-end"><span></span></div>$ {{ money_format('%i', $totalBilled) }} Billed</div>
                                    <div class="callout_text">
                                </div>
                            </div>
                            <div class="button_section"><a href="/payments" class="cws_button xlarge">View<div class="button-shadow"></div></a></div>
                        </div>
                    </div>
                </div>
                <!-- / callout -->
                <!-- callout -->
                <div class="grid_col grid_col_4">
                    <div class="ce clearfix">
                        <div class="cws_callout">
                            <div class="content_section">
                                <div class="separate"></div>
                                <div class="callout_title">
                                    <div class="bees bees-end"><span></span></div>
                                        {{ $totalChildren }}
                                        @if ($totalChildren > 1) Children @else Child @endif
                                    </div>
                                    <div class="callout_text">
                                </div>
                            </div>
                            <div class="button_section"><a href="/families" class="cws_button xlarge">View<div class="button-shadow"></div></a></div>
                        </div>
                    </div>
                </div>
                <!-- / callout -->
            </div>

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>
@endsection
