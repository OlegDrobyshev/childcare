@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'>
                <h1>Child: {{ $child->full_name }}</h1>
            </div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/families/" >Families</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <a href="/family/{{ $child->family_id }}" >The {{ $child->family->last_name }}s</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">{{ $child->full_name }}</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>

        <main>

            <!-- Edit form -->
            <div class='grid_row clearfix'>
                <div class='grid_col grid_col_3'>
                    <div class='ce clearfix'>

                        <h1 class="ce_title text-center">&nbsp;</h1>

                        @if ($child->photo)
                            <div class='media_part'>
                                <div class='pic'>
                                    <img class="border aligncenter image-type " src='{{ $child->photo }}' />
                                    <div class='links_popup animate'>
                                        <div class='link_cont'>
                                            <div class='link'>
                                                <a class='fancy' href='{{ $child->photo }}'><i class='fa fa-camera'></i></a>
                                                <div class='link-item-bounce'></div>
                                            </div>
                                            <div class='link'>
                                                <a href='#' id="change-photo"><i class='fa fa-pencil'></i></a>
                                                <div class='link-item-bounce'></div>
                                            </div>
                                        </div>
                                        <div class='link-toggle-button'>
                                            <i class='fa fa-plus link-toggle-icon'></i>
                                        </div>
                                    </div>
                                    <div class='hover-effect'></div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>

                <div class='grid_col grid_col_6'>
                    <div class='ce clearfix'>

                        <h1 class="ce_title text-center">{{ $child->full_name }}</h1>

                        @if (Session::has('message'))
                            {!! showMessage(Session::get('message')['type'], Session::get('message')['text']) !!}
                        @endif

                        {!! Form::open([
                            'url' => '/child/' . $child->id . '/update',
                            'enctype' => 'multipart/form-data',
                            'id' => 'edit-child-form'])
                        !!}
                            {!! Form::hidden('family_id', $child->family->id) !!}
                            {!! Form::hidden('child_id', $child->id) !!}

                            <div class="form-input">
                                {!! Form::text('name', $child->name, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Name'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::select('gender', ['male' => 'Male', 'female' => 'Female'], null, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'name'),
                                    'placeholder' => '* Gender'
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::text('dob', $child->dobHuman, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'dob'),
                                    'placeholder' => '* Birthday',
                                    'data-toggle' => 'datepicker',
                                    'data-dob' => $child->dob
                                ]) !!}
                            </div>

                            <div class="form-input" style="margin-top: 10px;">
                                {!! Form::file('photoFile', [
                                    'class' => 'form-control ' . getErrorClass($errors, 'photo'),
                                    'style' => '
                                        color: #aaa;
                                        padding: .5rem .75rem;
                                        line-height: 8px;
                                        font-size: 14px;
                                        cursor: pointer;
                                        border-radius: 8px;
                                        border: 1px solid rgba(0, 0, 0, .15);
                                    '
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::textarea('allergies', $child->allergies, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'allergies'),
                                    'placeholder' => 'Allergies',
                                    'rows' => 2
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::textarea('notes', $child->notes, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'notes'),
                                    'placeholder' => 'Notes',
                                    'rows' => 4
                                ]) !!}
                            </div>

                            <div class="form-input">
                                {!! Form::select('group_id', $groups, $child->group_id, [
                                    'class' => 'form-control ' . getErrorClass($errors, 'group_id'),
                                    'placeholder' => '* Group'
                                ]) !!}
                            </div>

                            <div class="form-input text-center" style="margin: 20px 0;">
                                <label for="Sun" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="Sun" type="checkbox" name="attendance[]" value="Sun" {{ checkDay($child->days, 'Sun') }}> Sun
                                </label>
                                <label for="mon" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="mon" type="checkbox" name="attendance[]" value="Mon" {{ checkDay($child->days, 'Mon') }}> Mon
                                </label>
                                <label for="tue" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="tue" type="checkbox" name="attendance[]" value="Tue" {{ checkDay($child->days, 'Tue') }}> Tue
                                </label>
                                <label for="wed" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="wed" type="checkbox" name="attendance[]" value="Wed" {{ checkDay($child->days, 'Wed') }}> Wed
                                </label>
                                <label for="thu" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="thu" type="checkbox" name="attendance[]" value="Thu" {{ checkDay($child->days, 'Thu') }}> Thu
                                </label>
                                <label for="fri" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="fri" type="checkbox" name="attendance[]" value="Fri" {{ checkDay($child->days, 'Fri') }}> Fri
                                </label>
                                <label for="Sat" style="display: inline-block; padding-right: 10px;">
                                    <input class="input-checkbox" id="Sat" type="checkbox" name="attendance[]" value="Sat" {{ checkDay($child->days, 'Sat') }}> Sat
                                </label>
                            </div>

                            <div class="form-input text-center">
                                <label for="duration-full-day" class="checkbox" style="display: inline-block; margin-right: 30px;">
                                    <input type="radio" name="full_day" id="duration-full-day" value="1" @if ($child->full_day) checked @endif /> Full Day
                                </label>
                                <label for="duration-half-day" class="checkbox" style="display: inline-block;">
                                    <input type="radio" name="full_day" id="duration-half-day" value="0" @if ( ! $child->full_day) checked @endif /> Half Day
                                </label>
                            </div>

                            <div class="form-input">
                                {!! Form::text('weekly_rate', $child->weekly_rate ?: '', [
                                    'class' => 'form-control ' . getErrorClass($errors, 'weekly_rate'),
                                    'placeholder' => '* Weekly Rate'
                                ] ) !!}
                            </div>
                            <div class="form-input">
                                {!! Form::text('discount', $child->discount ?: '', [
                                    'class' => 'form-control ' . getErrorClass($errors, 'discount'),
                                    'placeholder' => 'Discount in %'
                                ] ) !!}
                            </div>

                            <div class="form-input text-center">
                                <button type="submit" class="cws_button alt">
                                    <span class="cws_button_inner">Save</span>
                                </button>
                                <a href="/family/{{ $child->family_id }}" style="margin: 30px 0 0 10px;">Cancel</a>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div><!-- / Edit form -->

        </main>

        <div class='right-pattern pattern pattern-2'></div>

        <!-- footer container image / -->
        <div class="footer_image"></div>
    </div>

@endsection

@section('scripts')
    {!! Html::style('/assets/plugins/datepicker/datepicker.min.css') !!}

    {!! Html::script('/assets/plugins/datepicker/datepicker.min.js') !!}
    {!! Html::script('/assets/pages/child-edit.js') !!}
    <script>
        ChildEdit.init();
    </script>
@endsection