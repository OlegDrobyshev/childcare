@extends('layouts.app')

@section('breadcrumbs')

    <!-- breadcrumbs -->
    <section class='page_title wave'>
        <div class='container'>
            <div class='title'><h1>Blank</h1></div>

            <nav class="bread-crumbs">
                <a href="/" >Home</a>
                <i class="delimiter fa fa-chevron-right"></i>
                <span class="current">Current</span>
            </nav>
        </div>
        <canvas class='breadcrumbs' data-bg-color='#f8f2dc' data-line-color='#f9e8b2'></canvas>
    </section>
    <!-- / breadcrumbs -->

@endsection

@section('content')

    <div class="page_content">
        <div class='left-pattern pattern pattern-2'></div>
        <main>
            <div class='grid_row clearfix'>
                <div class='grid_col grid_col_6'>
                    <div class='ce clearfix'>
                        <h3 class="ce_title">Who We Are</h3>
                    </div>
                </div>
            </div>
        </main>
    </div>

@endsection