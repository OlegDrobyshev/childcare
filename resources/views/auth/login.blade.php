@extends('layouts.app-no-menu')

@section('content')

<div class="page_content">

    <div class='left-pattern pattern pattern-2'></div>

    <main>
        <!-- content section -->
        <div class='grid_row clearfix'>
            <div class='grid_col grid_col_6'>
                <div class='ce clearfix'>
                    <h3 class="ce_title">Sign In</h3>
                </div>
            </div>
        </div>

        <div class="grid_row clearfix" style="margin-bottom: 160px;">
            <div class="grid_col grid_col_12">
                <div class="ce clearfix">

                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="text-align: center;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form action="{{ url('/auth/login') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="grid_col grid_col_4">&nbsp;</div>

                        <div class="grid_col grid_col_4">
                            <p>
                                <label for="email">Email <span class="required">*</span></label>
                                <input id="email" name="email" type="email" value="{{ old('email') }}" size="30" aria-required="true" required="required">
                            </p>
                            <p>
                                <label for="password">Password <span class="required">*</span></label>
                                <input type="password" id="password" name="password" value="" size="30" aria-required="true" required="required">
                            </p>
                            <p>
                                <label for="">
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </p>
                            <br />
                            <input name="submit" type="submit" id="submit" class="submit aligncenter" value="Sign In">
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </main>

    <div class='right-pattern pattern pattern-2'></div>

    <!-- footer container image / -->
    <div class="footer_image"></div>
</div>
@endsection
