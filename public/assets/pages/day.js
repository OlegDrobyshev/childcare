var Day = function( $ ) {
    return {
        init: function( options ) {

            var table = $( "table" ),
                findChildSelect = $( "input#find-child-ajax" );

            /**
             * Enable attendance inputs
             */
            table.on( 'change', "input[name='attended']", function( e ) {
                var selects = $( this ).parents( "tr" ).find( "select.select" ),
                    status = $( this ).attr( "checked" ),
                    childId = $( this ).parents( "tr" ).data( "child-id" ),
                    data = {};

                status = ( status == "checked" ) ? 1 : 0;

                if ( status ) {
                    selects.select2( "enable" );
                } else {
                    selects.select2( "disable" );
                }

                data.child_id = childId;
                data.status = status;

                $.each( selects, function( key, el ) {
                    data[ $( el ).attr( "name" ) ] = $( el ).val();
                } );

                updateAttendance( data );
            } );

            /**
             * Update attendance values on change
             */
            table.on( "change", "select.select", function( e ) {
                var key = $( this ).attr( "name" ),
                    value = $( this ).val(),
                    data = {};

                data.child_id = $( this ).parents( "tr" ).data( "child-id" );
                data[ key ] = value;

                updateAttendance( data );
            } );

            /**
             * Update attendance ajax
             *
             * @param data
             */
            function updateAttendance(data) {
                data.date = options.day;
                $.post( "/attendance/update-ajax", data );
            }

            /**
             * Go to group table
             */
            $( "select[name='go-to-group']" ).on( "change", function( e ) {
                var id = $( this ).val() + "-group";

                $( "html, body" ).animate( {
                    scrollTop: $( "#" + id ).offset().top - 120
                }, 300);
            } );

            /**
             * Find child
             */
            findChildSelect.select2( {
                placeholder: "Child",
                minimumInputLength: 2,
                ajax: {
                    url: "/children/find-by-name-ajax",
                    dataType: 'json',
                    type: "POST",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    results: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.value,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            } );

            /**
             * Add child ajax
             */
            $( "a#add-child-btn" ).on( "click", function( e ) {
                e.preventDefault();

                if ( ! findChildSelect.val()) return false;

                $.post(
                    "/attendance/add-child-ajax", {
                        date: options.day,
                        childId: findChildSelect.val()
                    }, function( data ) {
                        window.location.reload();
                    }
                );
            } );

            /**
             * Highlight and scroll to newlly added child
             */
            (function() {
                if ( options.addedChildId == 0 ) return false;

                var childRow = $( "tr[data-child-id=" + options.addedChildId + "]" );
                childRow.css( {
                    background: "#fcf8e3"
                } );
                $( "html, body" ).animate( {
                    scrollTop: childRow.offset().top - 120
                }, 300);
            })();
        }
    };
} ( jQuery );