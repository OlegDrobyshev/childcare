var ChildEdit = function( $ ) {
    return {
        init: function( options ) {

            var photoInput = $( "input[name='photoFile']" );

            /**
             * Initialize datepickers
             */
            $( '[data-toggle="datepicker"]' ).datepicker( {
                autoHide: true,
                format: 'yyyy-mm-dd',
                date: $( this ).data( "dob" )
            } );

            $( "a#change-photo" ).on( "click", function( e ) {
                e.preventDefault();

                photoInput.click();
            } );
        }
    };
} ( jQuery );