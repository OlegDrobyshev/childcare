var Payments = function( $ ) {
    return {
        init: function( options ) {

            var toolbox = $( "div#toolbox" );

            /**
             * Delete payment prompt
             */
            $( "a.delete-payment" ).on( "click", function( e ) {
                e.preventDefault();
                if ( confirm( "Do you really want to delete payment?" ) ) {
                    window.location.href = $( this ).attr( "href" );
                }
            } );

            /**
             * Open accordion if form has errors
             */
            (function openErrorForm() {
                if ( options.errorForm ) {
                    setTimeout(function() {
                        toolbox.find( "div#" + options.errorForm ).click();
                    }, 300);
                }
            })();

            /**
             * Find family
             */
            $( "input#find-family-ajax" ).select2( {
                placeholder: "Family",
                minimumInputLength: 2,
                ajax: {
                    url: "/families/find-by-last-name-ajax",
                    dataType: 'json',
                    type: "POST",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    results: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.value,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            } );
        }
    };
} ( jQuery );