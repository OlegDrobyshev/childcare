var Family = function( $ ) {
    return {
        init: function( options ) {

            var toolbox = $( "div#toolbox" );

            /**
             * Open accordion if form has errors
             */
            (function openErrorForm() {
                if ( options.errorForm ) {
                    setTimeout(function() {
                        toolbox.find( "div#" + options.errorForm ).click();
                    }, 300);
                }
            })();

            /**
             * Delete parent prompt
             */
            $( "a.delete-parent" ).on( "click", function( e ) {
                e.preventDefault();
                if ( confirm( "Do you really want to delete " + $( this ).data( "name" ) + "?" ) ) {
                    window.location.href = $( this ).attr( "href" );
                }
            } );

            /**
             * Initialize datepickers
             */
            $( '[data-toggle="datepicker"]' ).datepicker( {
                autoHide: true,
                format: 'yyyy-mm-dd',
                date: new Date(),
                autoPick: true
            } );
        }
    };
} ( jQuery );