var Groups = function( $ ) {
    return {
        init: function( options ) {

            var toolbox = $( "div#toolbox" );

            console.log(toolbox, options);

            /**
             * Open accordion if form has errors
             */
            (function openErrorForm() {
                if ( options.errorForm ) {
                    setTimeout(function() {
                        toolbox.find( "div#" + options.errorForm ).click();
                    }, 300);
                }
            })();
        }
    };
} ( jQuery );