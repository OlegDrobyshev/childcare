var Families = function( $ ) {
    return {
        init: function( options ) {

            var searchForm = $( "form#search" );

            /**
             * Redirect to search URL
             */
            searchForm.on( "submit", function( e ) {
                e.preventDefault();

                var string = $(this ).find( "input[name='q']" ).val(),
                    encodedString = encodeURIComponent( string );

                window.location.href = "/families/search/" + encodedString;
            } );

            /**
             * Delete family prompt
             */
            $( "a.delete-family" ).on( "click", function( e ) {
                e.preventDefault();
                if ( confirm( "Do you really want to delete " + $( this ).data( "last_name" ) + " family?" ) ) {
                    window.location.href = $( this ).attr( "href" );
                }
            } );
        }
    };
} ( jQuery );